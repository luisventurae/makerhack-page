'use strict'

// const morgan = require('morgan')
const express = require('express')
const app = express()
const path = require('path')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const myconnection = require('express-myconnection')
const fileUpload = require('express-fileupload')
const router = require ('./routes/index')
const session = require('express-session')
const passport = require('passport')
const flash = require('connect-flash')

const port = process.env.PORT || 3334
const database = require('./app/config/database/')
const ssecret = require('./app/config/session/')
require('./app/config/passport')

// Settings
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

// Middlewares
// app.use(morgan('dev'))
app.use(express.static( path.join(__dirname, 'public') ))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(fileUpload())
app.use(myconnection(mysql, database, 'single')) 
app.use(session(ssecret))
app.use(flash())
app.use(passport.initialize())
app.use(passport.session())

// Gloval Variables
app.use((request, response, next) => {
    app.locals.success = request.flash('success')
    app.locals.message = request.flash('message')
    app.locals.user = request.user
    next()
})

app.use(session({
    secret : 'maker-hack',
    resave : false,
    saveUninitialized : false
}))

app.use((request, response, next) =>{
app.locals.success = request.flash('success')
app.locals.message = request.flash('message')
next()
})

// Routes
app.use(router)

// Starting Server
app. listen(port, () => {
    console.log(`Running on http://127.0.0.1:${port}`)
})
