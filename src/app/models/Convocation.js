'use strict'
//todos los atributos
function Convocation(id, image, area, title, place, date, time, start, end, description) {
  this.id = id || null // int
  this.image = image || null // null 120
  this.area = area || null // 20
  this.date = date || null // DATE
  this.title = title || null // 60
  this.place = place || null // 50
  this.time = time || null // TIME
  this.start = start || null // DATE
  this.end = end || null // DATE
  this.description = description || null // 350
}

Convocation.prototype.getId = function() {
  return this.id
}

Convocation.prototype.setId = function(id) {
  this.id = id
}

Convocation.prototype.getImage = function(){
  return this.image
}

Convocation.prototype.setImage = function(image){
  this.image = image
}

Convocation.prototype.getArea = function() {
  return this.area;
}

Convocation.prototype.setArea = function(area) {
  this.area = area;
}

Convocation.prototype.getTitle = function() {
  return this.title;
}

Convocation.prototype.setTitle = function(title) {
  this.title = title;
}

Convocation.prototype.getPlace = function() {
  return this.place;
}

Convocation.prototype.setPlace = function(place) {
  this.place = place;
}

Convocation.prototype.getDate = function() {
  return this.date;
}

Convocation.prototype.setDate = function(date) {
  this.date = date;
}

Convocation.prototype.getTime = function() {
  return this.time;
}

Convocation.prototype.setTime = function(time) {
  this.time = time;
}

Convocation.prototype.getStart = function() {
  return this.start;
}

Convocation.prototype.setStart = function(start) {
  this.start = start;
}

Convocation.prototype.getEnd = function() {
  return this.end;
}

Convocation.prototype.setEnd = function(end) {
  this.end = end;
}

Convocation.prototype.getDescription = function() {
  return this.description;
}

Convocation.prototype.setDescription = function(description) {
  this.description = description;
}

Convocation.prototype.equals = function(otherConvocation) {
  return otherConvocation.getId() == this.getId()
      && otherConvocation.getImage() == this.getImage()
      && otherConvocation.getArea() == this.getArea()
      && otherConvocation.getTitle() == this.getTitle()
      && otherConvocation.getPlace() == this.getPlace()
      && otherConvocation.getDate() == this.getDate()
      && otherConvocation.getTime() == this.getTime()
      && otherConvocation.getStart() == this.getStart()
      && otherConvocation.getEnd() == this.getEnd()
      && otherConvocation.getDescription() == this.getDescription()
}

Convocation.prototype.fill = function(newFields) {
  for (var field in newFields) {
      if (this.hasOwnProperty(field) && newFields.hasOwnProperty(field)) {
          if (this[field] !== 'undefined') {
              this[field] = newFields[field]
          }
      }
  }
}

module.exports = Convocation
