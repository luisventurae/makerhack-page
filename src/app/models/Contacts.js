'use strict'


function Contacts(id, name, email, career, cycle, description){

    this.id = id || null // INT
    this.name = name || null // 50
    this.email = email ||  null // 40
    this.career = career || null // 200
    this.cycle = cycle || null // 2
    this.description = description || null // 300
}

Contacts.prototype.getId = function(){
    return this.id
}

Contacts.prototype.setId = function(){
    this.id = id
}

Contacts.prototype.getName = function(){
    return this.name
}

Contacts.prototype.setName = function(){
    this.name = name
}

Contacts.prototype.getEmail = function(){
    return this.email
}

Contacts.prototype.setEmail = function(){
    this.email = email
}

Contacts.prototype.getCareer = function(){
    return this.career
}

Contacts.prototype.setCareer = function(){
    this.career = career
}

Contacts.prototype.getCycle = function(){
    return this.cycle
}

Contacts.prototype.setCycle = function(){
    this.cycle = cycle
}

Contacts.prototype.getDescription = function(){
    return this.description
}

Contacts.prototype.setDescription = function(){
    this.description = description
}

Contacts.prototype.equals = function(othersContacts){
    return othersContacts.getId() == this.getId()
        && othersContacts.getName() == this.getName()
        && othersContacts.getEmail() == this.getEmail()
        && othersContacts.getCareer() == this.getCareer()
        && othersContacts.getCycle() == this.getCycle()
        && othersContacts.getDescription() == this.getDescription()
}

Contacts.prototype.fill = function(newFields) {
    for (var field in newFields) {
        if (this.hasOwnProperty(field) && newFields.hasOwnProperty(field)) {
            if (this[field] !== 'undefined') {
                this[field] = newFields[field]
            }
        }
    }
}

module.exports = Contacts