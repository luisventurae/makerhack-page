'use strict'

function Investigators(id, image, name, position, description){

    this.id = id
    this.image = image
    this.name = name
    this.position = position
    this.description = description
}

Investigators.prototype.getId = function() {
    return this.id
}

Investigators.prototype.setId = function(id) {
    this.id = id
}

Investigators.prototype.getImage = function() {
    return this.image
}

Investigators.prototype.setImage = function(image){
    this.image = image
}

Investigators.prototype.getName = function() {
    return this.name
}

Investigators.prototype.setName = function(name) {
    this.name = name
}

Investigators.prototype.getPosition = function() {
    return this. position
}

Investigators.prototype.setPosition = function(position) {
    this.position = position
}

Investigators.prototype.getDescription = function() {
    return this.description
}

Investigators.prototype.setDescription = function(description) {
    this.description = description
}

Investigators.prototype.equals = function(otherInvestigators) {
    return otherInvestigators.getId() == this.getId()
        && otherInvestigators.getImage() == this.getImage()
        && otherInvestigators.getName() == this.getName()
        && otherInvestigators.getPosition() == this.getPosition()
        && otherInvestigators.getDescription() == this.getDescription()
}


Investigators.prototype.fill = function(newFields) {
    for (var field in newFields) {
        if (this.hasOwnProperty(field) && newFields.hasOwnProperty(field)) {
            if (this[field] !== 'undefined') {
                this[field] = newFields[field]
            }
        }
    }
}
  
module.exports = Investigators
