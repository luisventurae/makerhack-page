'use strict'

function Users(id, user, name, password, typeUser){

    this.id = id || null // INT
    this.user = user || null // 20
    this.name = name || null // 60
    this.password = password || null // 25
    this.typeUser = typeUser || null // 30
}

Users.prototype.getId = function(){
    return this.id
}

Users.prototype.setId = function(id){
    this.id = id
}

Users.prototype.getUser = function(){
    return this.user
}

Users.prototype.setUser = function(user){
    this.user = user
}

Users.prototype.getName = function(){
    return this.name
}

Users.prototype.setName = function(name){
    this.name = name
}

Users.prototype.getPassword = function(){
    return this.password
}

Users.prototype.setPassword = function(password){
    this.password = password
}

Users.prototype.getTypeUser = function(){
    return this.typeUser
}

Users.prototype.setTypeUser = function(typeUser){
    this.typeUser = typeUser
}

Users.prototype.equals = function(otherUsers){

    return otherUsers.getId() == this.getId()
        && otherUsers.getName() == this.getName()
        && otherUsers.getUser() == this.getUser()
        && otherUsers.getPassword() == this.getPassword()
        && otherUsers.getTypeUser() == this.getTypeUser()
}

Users.prototype.fill = function(newFields) {
    for (var field in newFields) {
        if (this.hasOwnProperty(field) && newFields.hasOwnProperty(field)) {
            if (this[field] !== 'undefined') {
                this[field] = newFields[field]
            }
        }
    }
}

module.exports = Users
      

 
