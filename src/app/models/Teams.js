'use strict'

function Teams(id, team, name, surname, email, phone, university, career, description, members, type, idConvocation ){
this.id = id || null // INT
this.team = team || null // (30)
this.name = name || null // (30)
this.surname = surname || null // (30)
this.email = email || null // (50)
this.phone = phone || null // char(9)
this.university = university || null //(60)
this.career = career || null // (35)
this.description = description || null // (200)
this.members = members || null // (200)
this.type = type || null // (12)
this.idConvocation = idConvocation || null // INT


}

Teams.prototype.getId = function() {
    return this.id
}

Teams.prototype.setId = function(id) {
    this.id = id
}

Teams.prototype.getTeam = function() {
    return this.team
}

Teams.prototype.setTeam = function(team) {
    this.team = team
}

Teams.prototype.getName = function() {
    return this.name
}

Teams.prototype.setName = function(name) {
    this.name = name
}

Teams.prototype.getSurname = function() {
    return this.surname
}

Teams.prototype.setSurname = function(surname) {
    this.surname = surname
}

Teams.prototype.getEmail = function() {
    return this.email
}

Teams.prototype.setEmail = function(email) {
    this.email = email
}

Teams.prototype.getPhone = function() {
    return this.phone
}

Teams.prototype.setPhone = function(phone) {
    this.phone = phone
}

Teams.prototype.getUniversity = function() {
    return this.university
}

Teams.prototype.setUniversity = function(university){
    this.university = university
}

Teams.prototype.getCareer = function() {
    return this.career
}

Teams.prototype.setCareer = function(career) {
    this.career = career
}

Teams.prototype.getDescription = function(){
    return this.description
}

Teams.prototype.setDescription = function(description){
    this.description = description
}

Teams.prototype.getMembers = function(){
    return this.members
}

Teams.prototype.setMembers = function(members){
    this.members = members
}

Teams.prototype.getType = function(){
    return this.type
}

Teams.prototype.setType = function(type){
    this.type = type
}

Teams.prototype.getIdConvocation = function(){
    return this.idConvocation
}

Teams.prototype.setIdConvocation = function(){
    this.idt = idConvocation
}

Teams.prototype.equals = function(otherTeams) {
    return otherTeams.getId() == this.getId()
        && otherTeams.getTeam()  == this.getTeam()
        && otherTeams.getName() == this.getName()
        && otherTeams.getSurname() == this.getSurname()
        && otherTeams.getEmail() == this.getEmail()
        && otherTeams.getPhone() == this.getPhone()
        && otherTeams.getCareer() == this.getCareer()
        && otherTeams.getUniversity() == this.getUniversity()
        && otherTeams.getDescription() == this.getDescription()
        && otherTeams.getMembers() == this.getMembers()
        && otherTeams.getType() == this.getType()
        && otherTeams.getIdConvocation() == this.getIdConvocation()
}


Teams.prototype.fill = function(newFields) {
    for (var field in newFields) {
        if (this.hasOwnProperty(field) && newFields.hasOwnProperty(field)) {
            if (this[field] !== 'undefined') {
                this[field] = newFields[field]
            }
        }
    }
}
  
module.exports = Teams