'use strict'
function News(id, image, title, description, files) {
    this.id = id || null // INT
    this.image = image || null // 120
    this.title = title || null // 50
    this.description = description || null // 500
    this.files = files || null // 120
}

News.prototype.getId = function() {
    return this.id
}

News.prototype.setId = function(id) {
    this.id = id
}
  
News.prototype.getImage = function() {
    return this.image
}

News.prototype.setImage = function(image) {
    this.image = image
}

News.prototype.getTitle = function() {
    return this.title
}

News.prototype.setTitle = function(title) {
    this.title = title
}
  
News.prototype.getDescription = function() {
    return this.description
}

News.prototype.setDescription = function(description) {
    this.description = description
}
  
News.prototype.getFiles = function() {
    return this.files
}

News.prototype.setFiles = function(files) {
    this.files = files
}

News.prototype.equals = function(otherNews) {
    return otherNews.getId() == this.getId()
        && otherNews.getImage() == this.getImage()
        && otherNews.getTitle() == this.getTitle()
        && otherNews.getDescription() == this.getDescription()
        && otherNews.getFiles() == this.getFiles()
}
  
News.prototype.fill = function(newFields) {
    for (var field in newFields) {
        if (this.hasOwnProperty(field) && newFields.hasOwnProperty(field)) {
            if (this[field] !== 'undefined') {
                this[field] = newFields[field]
            }
        }
    }
}
  
module.exports = News