'use strict'

const insertNews = async (request, news, callback) => {
    
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
        if (err) return reject({ err: true, message: err })

        return conn.query('INSERT INTO `INNOUA_NOTIC`(`INNOUA_IMG`, `INNOUA_TITUL`, `INNOUA_DESCRIP`, `INNOUA_ARCH`) VALUES (?,?,?,?)',
            [
                news.getImage(),
                news.getTitle(),
                news.getDescription(),
                news.getFiles()
            ], (err, res) => {
                if (err) 
                    reject({err: true, message: err })

                    resolve({ res: res })
            })
    })
})
}

const getNews = async (request, callback) => {
    return new Promise((resolve, reject) => {
    request.getConnection((err, conn) => {
        if (err) reject({ err: true, message: err })
        conn.query('SELECT * FROM `INNOUA_NOTIC`', (err, data) => {
            if (err) {
                reject({ err: true, message: err })
            }
            resolve({ data: data })
        })
    })
})
}

const showLast4News = async (request) => {
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if (err) reject({err: true, message: err}) 
            conn.query('SELECT * FROM `INNOUA_NOTIC` ORDER BY `INNOUA_ID_N` DESC LIMIT 4', (err, data) => {
                if (err){
                    reject({err: true, message: err}) 
                }
                resolve({data: data}) 
            })
        })        
    })
}

const getNew = (request, id, callback) => {
    return new Promise((resolve, reject) => {
    request.getConnection((err, conn) => {
        if (err) reject({ err: true, message: err })
        conn.query('SELECT * FROM `INNOUA_NOTIC` WHERE `INNOUA_ID_N` ="?"', id
            , (err, data) => {
                if (err) reject({ err: true, message: err })
                resolve({ data: data })
            })
        })
    })
}

const alterNews = (request, news, callback) => {
    request.getConnection((err, conn) => {
        if (err) callback({ err: true, message: err })
        conn.query('UPDATE `INNOUA_NOTIC` SET `INNOUA_IMG`=?,`INNOUA_TITUL`=?,`INNOUA_DESCRIP`=?,`INNOUA_ARCH`=? WHERE `INNOUA_ID_N`="?"',
            [
                news.getImage(),
                news.getTitle(),
                news.getDescription(),
                news.getFiles(),
                news.getId()
            ], (err, res) => {
                if (err) callback({ err: true, message: err })
                callback({ res: res })
            })
    })
    conn.end()

}

module.exports = {
    insertNews,
    alterNews,
    getNew,
    getNews,
    showLast4News
}
