'use strict'

const insertConv = async (request, convocation) => {
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err}) 
    
            conn.query('INSERT INTO `INNOUA_CONV` (`INNOUA_IMAG`, `INNOUA_AREA`, `INNOUA_TITULO`, `INNOUA_LUG`, `INNOUA_FECH_EVE`, `INNOUA_HOR_EVE`, `INNOUA_INI_CONV`, `INNOUA_CIER_CONV`, `INNOUA_DESCRIPCION`) VALUES (?,?,?,?,?,?,?,?,?)',
            [   
                convocation.getImage(),
                convocation.getArea(),
                convocation.getTitle(),
                convocation.getPlace(),
                convocation.getDate(),
                convocation.getTime(),
                convocation.getStart(),
                convocation.getEnd(),
                convocation.getDescription()
            ],(err, res) => {    
                if (err){
                    reject({err: true, message: err}) 
                }
               
                resolve({res: res}) 
            })
    
        })
        // console.log(conn.close())
    })
}
/*const getTeam = (request, callback) => {
    request.getConnection((err, conn) => {
        if(err) callback({err: true, message: err})

        conn.query('SELECT `INNOUA_EQUIP` FROM `INNOUA_CONVOC WHERE INNOUA_EQUIP="?"'  , (err,data) => {
            if(err) {
                callback({err: true, message: err})
            }    
            callback({data: data})
        })
    })
}
*/

const getConvocations = async (request) => {
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err}) 
    
            conn.query('SELECT * FROM `INNOUA_CONV`', (err, data) => {
                if(err){
                    reject({err: true, message: err}) 
                }
                resolve({data: data}) 
            })
        })
    })
}

const showLast4Convocations = async (request) => {
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err}) 
                    conn.query('SELECT * FROM `INNOUA_CONV` ORDER BY `INNOUA_ID` DESC LIMIT 4', (err, data) => {
                    if(err){
                        reject({err: true, message: err}) 
                    }
                    resolve({data: data}) 
                })
            
    })
})
}

const getConvocation = async ( request, id) => {
    return new Promise((resolve, reject) => {        
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err}) 
    
            conn.query('SELECT *  FROM `INNOUA_CONV` WHERE `INNOUA_ID`= ?' ,            
                id
            , (err, data) =>{
                if(err) reject({err: true, message: err}) 
                return resolve({data: data}) 
            })
        })
    })
}


/*const getTeams = (request, callback) => {
    request.getConnection((err, conn) => {
        if(err) callback({err: true, message: err})

        conn.query('SELECT `INNOUA_EQUIP` FROM `INNOUA_CONVOC', (err, data) => {
            if(err){
                callback({err: true, message: err})
            }
            callback({data: data})
        })
    })
}
*/

/*
const alterTeam = (request, convocation, callback) => {
    request.getConnection((err, conn) => {
        if(err) callback({err: true, message: err})

        conn.query('UPDATE `INNOUA_CONVOC` SET INNOUA_EQUIP`="?" WHERE INNOUA_ID_C="?"',
        [   convocation.equip,
            convocation.id
        ],(err, res) => {
            if(err){            
                callback({err: true, message: err})
            }
            callback({res: res})
        })
    })
}
*/

const alterConvocation  = async (request, convocation) => {
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err})
            conn.query('UPDATE `INNOUA_CONV` SET `INNOUA_IMAG`=?,`INNOUA_AREA`=?,`INNOUA_TITULO`=?,`INNOUA_LUG`=?,`INNOUA_FECH_EVE`=?,`INNOUA_HOR_EVE`=?,`INNOUA_INI_CONV`=?,`INNOUA_CIER_CONV`=?,`INNOUA_DESCRIPCION`=? WHERE `INNOUA_ID`="?"',
            [
                convocation.getImage(),
                convocation.getArea(),
                convocation.getTitle(),
                convocation.getPlace(),
                convocation.getDate(),
                convocation.getTime(),
                convocation.getStart(),
                convocation.getEnd(),
                convocation.getDescription()
            ], (err, res) => {

                if (err){
                reject({err: true, message: err})
                }
                resolve({res: res})
            })
        })
    })
    
}


module.exports = {
    insertConv,
    getConvocations,
    getConvocation,
    showLast4Convocations,
    alterConvocation
}
