'use strict'

const insertContact = async (request, contacts) => {
    return new Promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err}) 
    
            conn.query('INSERT INTO `INNOUA_CONTAC`(`INNOUA_NOMB`, `INNOUA_EMAIL`, `INNOUA_CARRERA`, `INNOUA_CICLO`, `iNNOUA_DESCRIPCION`)  VALUES (?,?,?,?,?)',
            [
                contacts.getName(),
                contacts.getEmail(),
                contacts.getCareer(),
                contacts.getCycle(),
                contacts.getDescription()
    
            ],(err, res) => {
                if(err) reject({err: true, message: err}) 
                          
                //console.log({res: res})
                return resolve({res: res})
            })
        })
    })
    
} 

module.exports = {
    insertContact
}