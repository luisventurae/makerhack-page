'use strict'

const insertTeam = async (request, teams) => {
    return new Promise((resolve, reject) => {
    request.getConnection((err, conn) => {
        if(err) reject({err: true, message: err})
        conn.query('INSERT INTO `INNOUA_EQU`(`INNOUA_EQUI`, `INNOUA_NOM`, `INNOUA_APE`, `INNOUA_EMAIL`, `INNOUA_TEL`, `INNOUA_UNIVERSIDAD`, `INNOUA_CARRE`, `INNOUA_DESCRIPCION`, `INNOUA_MIEMBROS`, `INNOUA_TIPO`, `INNOUA_ID`) VALUES (?,?,?,?,?,?,?,?,?,?,?)',
        [
             teams.getTeam(),
             teams.getName(),
             teams.getSurname(),
             teams.getEmail(),
             teams.getPhone(),
             teams.getUniversity(),
             teams.getCareer(),
             teams.getDescription(),
             teams.getMembers(),
             teams.getType(),
             teams.getIdConvocation()
            
        ],(err, res) => {
            if(err) {
                reject({err: true, message: err})
            }
                resolve({res: res})
        })
    })
})
   
}

module.exports ={
    insertTeam
}