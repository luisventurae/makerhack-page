'use strict'


const show5Investigators = async (request) => {
    return new promise((resolve, reject) => {
        request.getConnection((err, conn) => {
            if(err) reject({err: true, message: err})

            conn.query('SELECT `INNOUA_ID_IN`,`INNOUA_IMAGEN`,`INNOUA_NOMBRE`,`INNOUA_CARGO`,`INNOUA_DESCRIPCION` FROM `INNOUA_INVEST` ORDER BY `INNOUA_ID_IN` ASC LIMIT 7',(err, data) => {
                if(err){
                    reject({err: true, message: err})
                }
                resolve({data: data})
            })
        })
    })
}

module.exports = {
    show5Investigators
}