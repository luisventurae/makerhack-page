'use strict'

const saveUser = async(request, user) => {
    return new Promise((resolve, reject) => {
    request.getConnection((err, conn) => {
        if(err) reject({err: true, message: err})
        
        conn.query('INSERT INTO `INNOUA_USU` ( `INNOUA_USUA`, `INNOUA_NOMBRE`, `INNOUA_PASS`, `INNOUA_TIPO_USU`) VALUES (?, ?, ?, ?)',
        [
            user.getUser(),
            user.getName(),
            user.getPassword(),
            user.getTypeUser()
        ],(err, res) =>{
            if (err){
                reject({err: true, message: err})
            }
            resolve({res: res})
            })
        })
    })
}

const searchUserById = async(request, id ) => {
    return new Promise((resolve, reject) => {
    request.getConnection((err, conn) => {
        if(err) reject({err: true, message: err})
        
        conn.query('SELECT * FROM `INNOUA_USU` WHERE `INNOUA_ID_U` = ?',
            id
        ,(err, data) =>{
            if (err){
                reject({err: true, message: err})
            }
            resolve({data: data})
            })
        })
    })
}

const searchUserByUsername = async(request, username ) => {
    return new Promise((resolve, reject) => {
    request.getConnection((err, conn) => {
        if(err) reject({err: true, message: err})
        
        conn.query('SELECT * FROM `INNOUA_USU` WHERE `INNOUA_USUA` = ?',
        username
        ,(err, data) =>{
            if (err){
                reject({err: true, message: err})
            }
            resolve({data: data})
            })
        })
    })
}

module.exports = {
    saveUser,
    searchUserById,
    searchUserByUsername
}