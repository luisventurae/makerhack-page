'use strict'

const Convocation = require('../models/Convocation')
const announCrudControll = require('../crud/announCrudController')

const index = async (request, response) => {
    let id = request.params.id

    try {
        let res = await announCrudControll.getConvocation(request, id) 
        request.session.loadIdAnnoun = id
        res.data.forEach(function (a) {
    
            let tittle = a.INNOUA_TITULO
            let career = a.INNOUA_AREA
            let place = a.INNOUA_LUG
            let description = a.INNOUA_DESCRIPCION
            let image = a.INNOUA_IMAG
            let hours = a.INNOUA_HOR_EVE.substr(0, 5)
    
            let eventDay = a.INNOUA_FECH_EVE.getDate()
            let eventMonth = a.INNOUA_FECH_EVE.getMonth() + 1
            let eventYear = a.INNOUA_FECH_EVE.getFullYear()
    
            console.log(a.INNOUA_FECH_EVE)
    
            let month = a.INNOUA_CIER_CONV.getMonth() + 1
            let day = a.INNOUA_CIER_CONV.getDate()
            let year = a.INNOUA_CIER_CONV.getFullYear()
    
            let month2 = a.INNOUA_INI_CONV.getMonth() + 1
            let day2 = a.INNOUA_INI_CONV.getDate()
            let year2 = a.INNOUA_INI_CONV.getFullYear()
    
            let months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo",
                "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"]
    
    
            return response.render('convocationRegister', {
                id: id,
                tittle: tittle,
                hours: hours,
                career: career,
                place: place,
                description: description,
                image: image,
    
                day: day,
                month: months[month],
                year: year,
    
                day2: day2,
                month2: months[month2],
                year2: year2,
    
                eventDay: eventDay,
                eventMonth: months[eventMonth],
                eventYear: eventYear
            })
        })

    } catch (error) {
        console.log(error)
        return response.render('error',{msg: 'Algo salió mal... alv :\'v'})
    }

}
const saveAnnoun = async (request, response) => {

    let image = request.files['imagen']
    let area = request.body.area
    let title = request.body.title
    let place = request.body.place
    let date = request.body.date
    let time = request.body.time
    let start = request.body.start
    let end = request.body.end
    let description = request.body.description

    


    let palabra = ""

    for (let i = 0; i <= 10; i++) {
        let numero = Math.floor(Math.random() * 25)
        let letraAleatoria = String.fromCharCode(97 + numero)
        var num = (Math.round(Math.random()* 10)).toString()
        palabra = palabra + letraAleatoria + num
    }
    
    //    console.log(palabra)

    let srcroute = 'src/public/uploads'
    let routeImage = `${srcroute}/images/${palabra}-${image['name']}`

    let imgStatic = `/uploads/images/${palabra}-${image['name']}`

    try {
        await image.mv(routeImage)
        console.log('Se subió la imagen')

        let convocation = new Convocation(null, imgStatic, area, title, place, date, time, start, end, description)
        console.log(convocation)

        await announCrudControll.insertConv(request, convocation)
        return response.render('./Admin/createAnnounDetail', {
            image: routeImage,
            area: area,
            title: title,
            place: place,
            date: date,
            time: time,
            start: start,
            end: end,
            description: description
        })

    } catch (error) {
        console.log(error)
        return response.render('error',{msg: 'Algo salió mal... alv :\'v'})
    }

}

const showConvocations = async (request, response) => {
    try {
        let res = await announCrudControll.getConvocations(request)
        return response.render('convocations', {
            data: res.data
        })
    } catch (error) {
        console.log(error)
        return response.render('error',{msg: 'Algo salió mal... alv :\'v'})
    }
}

module.exports = {
    index,
    saveAnnoun,
    showConvocations,
}