'use strict'

const testCrudController = require('../crud/testCrudController')
const announCrudController = require('../crud/announCrudController')
const convocation = require('../models/Convocation')
const news = require('../models/News')

function showConsole(request, response) {    
    let equipo = request.body.equipo;
    let telefono = request.body.telefono;
    if(!equipo || !telefono) { response.status(404).send('Por favor llenar todos los campos') }
    else { console.log(request.body) }
    
    return testCrudController.getConvocations(request, function(teams) {
        // console.log(teams)

        return testCrudController.getNombre(request, function(res) {
            console.log('en el callback')        
            console.log(`El nombre es: ${res}`)
            saveConvocation(request, response)
            return response.render('detailsCon', {equipo : equipo , telefono: telefono})
        }) 
    })
   
}

function saveConvocation(request, response) {    
    let area = "Luis"
    let title = "Hackathon"
    let place = "San Isidro"
    let date = "2019-04-01"
    let time = "12:12"
    let start = "2019-04-02"
    let end = "2019-04-03"
    let description = "El concurso sera chevere"

    let convocation = new Convocation(null, area, title, place, date, time, start, end, description)
    console.log(convocation)
    
    return announCrudController.insertTeam(request, convocation, function(res) {
        console.log(res)
    })


}

function saveNews(request,response){

    let image = ""
    let title = ""
    let description = ""
    let files = ""

    let news = new news(null,image ,title ,description ,files)
    console.log(news)

}



module.exports = {
    showConsole,
    saveConvocation,
    saveNews
}
