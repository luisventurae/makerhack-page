'use strict'

const nodemailer = require("nodemailer")
const contactsController = require('../crud/contactsCrudController')
const Contacts = require('../models/Contacts')

const saveContacts = async (request, response) => {

    let name = request.body.name
    let email = request.body.email
    let career = request.body.career
    let cicle = request.body.cicle
    let description = request.body.description

    let contacts = new Contacts(null, name, email, career, cicle, description)

    // console.log(contacts)

    try {
        await contactsController.insertContact(request, contacts)  
        // await sendMail(name, email)      
        return response.redirect('/')

    } catch (error) {
        console.log(error)
        return response.render('error',{msg: 'Algo salió mal... alv :\'v'})

    }

}

// const sendMail = async (nameFrom, emailFrom) => {
//     return new Promise((resolve, reject) => {
//         // Generate test SMTP service account from ethereal.email
//         // Only needed if you don't have a real mail account for testing
//         let testAccount = await nodemailer.createTestAccount();
    
//         // create reusable transporter object using the default SMTP transport
//         let transporter = nodemailer.createTransport({
//             host: "smtp.gmail.com",
//             port: 465,
//             secure: true, // true for 465, false for other ports
//             auth: {
//                 user: testAccount.user, // generated ethereal user
//                 pass: testAccount.pass // generated ethereal password
//             }
//         });
    
//         // send mail with defined transport object
//         let info = await transporter.sendMail({
//             from: `"${nameFrom}" <${emailFrom}>`, // sender address
//             to: "luisigsystem@gmail.com", // list of receivers
//             subject: "Hello ✔", // Subject line
//             text: "Hello world?", // plain text body
//             html: "<b>Hello world?</b>" // html body
//         });
    
//         console.log("Message sent: %s", info.messageId);
//         // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    
//         // Preview only available when sending through an Ethereal account
//         console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//         // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

//     })
// }

module.exports = {
    saveContacts
}