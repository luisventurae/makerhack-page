`user strict`

const Teams = require('../models/Teams')
const teamsCrudController = require('../crud/teamsCrudController')

const saveTeam = async(request, response) =>{
    console.log(request.session.loadIdAnnoun)
    let idConvocation = request.session.loadIdAnnoun
    let name = request.body.name
    let email = request.body.email
    let phone = request.body.phone
    let university = request.body.university
    let career = request.body.career

    let teams = new Teams(null, null, name, null, email, phone, university, career, null, null, null, idConvocation)
    console.log(teams)
    try {
        await teamsCrudController.insertTeam(request, teams)
        return response.redirect(`/convocatoria/${idConvocation}`)
        
    } catch (error) {
        console.log(error)
        return response.render('error',{msg: 'Algo salió mal... alv :\'v'})
        
    }
   
}

module.exports = {
    saveTeam
}