'use strict'

const News = require('../models/News')
const newsCrudController = require('../crud/newsCrudController')

const saveNews = async (request, response) => {

    let palabra = ""
    for (let i = 0; i <= 10; i++) {
        let numero = Math.floor(Math.random() * 25)
        let letraAleatoria = String.fromCharCode(97 + numero)
        var num = (Math.round(Math.random()* 10)).toString()
        palabra = palabra + letraAleatoria + num
    }

    let image = request.files['imagen']
    let title = request.body.title
    let description = request.body.description
    let pdf = request.files['pdf']

    let route = 'src/public/uploads'
    let routeImage = `${route}/images/${image['name']}`
    let imgStatic = `/uploads/images/${image['name']}`
    let routePDF = `${route}/documents/${pdf['name']}`
    let pdfStatic = `/uploads/documents/${pdf['name']}`

    try {
        await image.mv(routeImage)
        console.log('Se subió la imagen')
        await pdf.mv(routePDF)
        console.log('Se subió el documento')
        let news = new News(null, imgStatic, title, description, pdfStatic)
        console.log(news)
        await  newsCrudController.insertNews(request, news)
        return response.render('./Admin/createNewsDetail', {
            image : routeImage, 
            title : title, 
            description : description,
            pdf : routePDF
        })   
    } catch (error) {
          console.log(error)
    }
    return image.mv(routeImage, function(err) {
        if(err) return console.log(err)
        console.log('Se subió la imagen')

        return pdf.mv(routePDF, function(err) {
            if(err) return console.log(err)
            console.log('Se subió el documento')

            let news = new News(null, imgStatic, title, description, pdfStatic)
            console.log(news)
            return newsCrudController.insertNews(request, news, function(res) {  
                if(res.err) {
                    console.log(res.message)
                    return response.send('Algo salio mal')        
                }
                return response.render('./Admin/createNewsDetail', {
                    image : routeImage, 
                    title : title, 
                    description : description,
                    pdf : routePDF
                })
            })

        })
    })
}
 
module.exports = {
    saveNews
}

