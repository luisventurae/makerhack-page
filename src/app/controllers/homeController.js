'use strict'

const announCrudController = require('../crud/announCrudController')
const newsController = require('../crud/newsCrudController')



const index = async (request,response) => {
    try {
        let res1 = await announCrudController.showLast4Convocations(request)
        let res2 = await newsController.showLast4News(request)
        console.log(res1)
        // console.log(res2)
        return response.render('index', {
                data1: res1.data,
                data2: res2.data
        })
    } catch (error) {
        console.log(error)
        return response.render('error', { msg: 'Algo salió mal... alv :\'v' })
    }
}

// const index = (request,response) => {

//     announCrudController.showLast4Convocations(request, function (res1){

//         if (res1.err) return response.render('error', { msg: 'Algo salió mal... alv :\'v' })
            
// 		newsController.showNews(request, function (res2) {
// 			if (res2.err) return response.render('error', { msg: 'Algo salió mal... alv :\'v' })
            
// 			return response.render('index', {
// 					data1: res1.data,
// 					data2: res2.data
// 			})
// 		})
//     } )
// }

    // const index = (request, response) => {

    //     return announCrudController.showLast4Convocations(request, function (res) {

    //         if (res.err) return response.render('error', { msg: 'Algo salió mal... alv :\'v' })

    //         return response.render('index', {
    //             data: res.data,
    //         })
    //     })
    // }

module.exports = {
    index
}