'use strict'

const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
var util = require('util')

const User = require('../../models/Users')
const userCrudController = require('../../crud/usersCrudController')
const helpers = require('./helpers')

passport.use('local.signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (request, username, password, done) => {
    try {
        console.log(request.body)    

        let row = await userCrudController.searchUserByUsername(request, username)
        
        if (row.data.length > 0) {
            let user = new User(row.data[0].INNOUA_ID_U, row.data[0].INNOUA_USUA, row.data[0].INNOUA_NOMBRE, row.data[0].INNOUA_PASS, row.data[0].INNOUA_TIPO_USU)
            const validPassword = await helpers.matchPassword(password, user.getPassword())
            if (validPassword) {
                return done(null, user, request, request.flash('success', 'welcome'))
            } else {
                return done(null, false, request.flash('message', 'el usuario o clave es incorrecto'))
            }   
        }else{
            return done(null, false, request.flash('message', 'el usuario no existe'))
        }
    } catch (error) {
        console.log(error)
        return response.render('error', { msg: 'Algo salió mal... alv :\'v' })
    }
}))


passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (request, username, password, done) => {
    // console.log(request.body)
    const { name } = request.body
    const type = 'Admin'    
    // console.log(request)
    console.log('------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------')

    try {
        let newUser = new User(null, username, name, null, type)
        newUser.setPassword(await helpers.encryptPassword(password))
        const result = await userCrudController.saveUser(request, newUser)
        // console.log(result)
        newUser.setId(result.res.insertId)
        return done(null, newUser, request)
    } catch (error) {
        console.log(error)
        return done(null, false, request.flash('message', 'no se pudo crear al usuario, posiblemente ya existe uno con el mismo usuario'))
    }
}))

passport.serializeUser( async (request, user, done) => {
     const id = await user.getId()
    done(null, id, request)
})

passport.deserializeUser( async (request, id, done) => {
    const row = await userCrudController.searchUserById(request, id)
    // console.log(row.data)
    done(null, row.data[0])
})
