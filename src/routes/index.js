'use strict'

const express = require('express')
const router = express.Router() 
const passport = require('passport')

const announController = require('./../app/controllers/announController')
const newsController = require('./../app/controllers/newsController')
const teamController = require('./../app/controllers/teamController')
const homeController = require('./../app/controllers/homeController')
const contactController = require('./../app/controllers/contactController')

// Public
router.get('/', (request, response) => {
    return homeController.index(request, response)
})

router.post('/', (request, response) => {

    return contactController.saveContacts(request, response)
})

router.get('/contacto', (request, response) => {
    return response.render('contact')
})

router.get('/crear/convocatoria', (request, response) => {
    return response.render('./Admin/createAnnoun')
})

router.post('/crear/convocatoria', (request, response) => {    
    return announController.saveAnnoun(request, response)
})

router.post('/convocatoria/participar', (request, response) => {    
    return teamController.saveTeam(request, response)
})

router.get('/crear/noticia', (request , response) => {
    return response.render('./Admin/createNews')
})

router.post('/crear/noticia', (request, response) => {
    return newsController.saveNews(request, response)
})
router.get('/convocatorias', (request, response) => {
    return announController.showConvocations(request, response)        
})

router.get('/registros', (request, response) => {
   return response.render('register')
    // return registerController.saveRegisters(request, response)
})
    
router.get('/convocatoria/:id', (request, response) => {
    return announController.index(request, response)
})

router.get('/admin' , (request, response) => {
    return response.render('./Admin/main')
})

// Login y Signup
router.get('/start/admin' , (request, response) => {
    return response.render('./Admin/login', {message:request.flash('loginMessage')})
})

router.post('/start/admin',  passport.authenticate('local.signin', {
        successRedirect : '/admin',
        failureRedirect : '/start/admin',
        failureFlash : true
    })
)

router.get('/start/admin/signup' , (request, response) => {
    return response.render('./Admin/signup') 
})

router.post('/start/admin/signup', passport.authenticate('local.signup', {
        successRedirect: '/admin',
        failureRedirect: '/start/admin/signup',
        failureFlash: true
    })
)

// Control de Errores
router.get('*', function(req, res){
    return res.status(404).render('error', {msg: 'No se ha encontrado esta página'})
});

module.exports = router 